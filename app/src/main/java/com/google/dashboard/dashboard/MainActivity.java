package com.google.dashboard.dashboard;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int NUM_PAGES = 8;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPager = findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }
        //TelefonoFrg fragment = new TelefonoFrg();
        @Override
        public Fragment getItem(int position) {
            if(position>=0 && position <= 7){
                TelefonoFrg fragment = new TelefonoFrg();
                fragment.setPosition(position);
                return fragment;
            } else
                //just in case
                return new Fragment();
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public void onMenuClick(View view) {

        switch (mPager.getCurrentItem()) {
            case 0:
                //Intent toTel = new Intent(this, TelefonoActivity.class);
                //startActivity(toTel);
                Toast.makeText(this, "Activity Telefono", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                Toast.makeText(this, "Activity Family", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(this, "Activity Camera", Toast.LENGTH_SHORT).show();
                break;
            case 3:
                Toast.makeText(this, "Activity Health", Toast.LENGTH_SHORT).show();
                break;
            case 4:
                Toast.makeText(this, "Activity Navigation", Toast.LENGTH_SHORT).show();
                break;
            case 5:
                Toast.makeText(this, "Activity Clock", Toast.LENGTH_SHORT).show();
                break;
            case 6:
                Toast.makeText(this, "Activity Radio", Toast.LENGTH_SHORT).show();
                break;
            case 7:
                Toast.makeText(this, "Activity SOS", Toast.LENGTH_SHORT).show();
                break;
        }

    }
}
