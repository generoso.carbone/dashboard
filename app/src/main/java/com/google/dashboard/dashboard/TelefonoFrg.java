package com.google.dashboard.dashboard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class TelefonoFrg extends Fragment{

    private int position;

    public void setPosition(int position){ this.position = position;}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.fragment_telefono_frg, container, false);
        TextView tv = v.findViewById(R.id.textView12);

        switch (position){
            case 0:
                tv.setText("TELEFONO");
                break;
            case 1:
                tv.setText("FAMIGLIA");
                break;
            case 2:
                tv.setText("FOTOCAMERA");
                break;
            case 3:
                tv.setText("SALUTE");
                break;
            case 4:
                tv.setText("NAVIGAZIONE");
                break;
            case 5:
                tv.setText("SVEGLIA");
                break;
            case 6:
                tv.setText("RADIO");
                break;
            case 7:
                tv.setText("SOS");
                break;
        }
        return v;

        //return rootView;
    }
}
